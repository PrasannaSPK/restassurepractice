package com.apimodals;

import com.apisupportingmodals.AddressPOJO;
import com.apisupportingmodals.CompanyPOJO;

public class UserPOJO {
	private int id;
	private String name;
	private String username;
	private String email;
	private AddressPOJO address;
	private String phone;
	private String website;
	private CompanyPOJO company;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public AddressPOJO getAddress() {
		return address;
	}

	public void setAddress(AddressPOJO address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public CompanyPOJO getCompany() {
		return company;
	}

	public void setCompany(CompanyPOJO company) {
		this.company = company;
	}
}
