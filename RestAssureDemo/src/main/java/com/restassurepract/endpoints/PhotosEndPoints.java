package com.restassurepract.endpoints;

public class PhotosEndPoints {

	public static final String GET_ALL_PHOTOS="/photos";
	public static final String GET_SPECIFIC_PHOTOS="/photos/";
	public static final String UPDATE_SPECIFIC_PHOTOS="/photos/";
	public static final String CREATE_PHOTOS="/photos";
}
