package com.restassurepract.endpoints;

/**
 * @author sodhaprasanna.k
 * This class contains end points for users module
 *
 */
public class UsersEndPoints {

	public final static String GET_ALL_USERS="/users";
	public final static String GET_SINGLE_USER="/users/";
	public final static String CREATE_USER="/users";
	public final static String UPDATE_USER="/users/";
	
	
}
