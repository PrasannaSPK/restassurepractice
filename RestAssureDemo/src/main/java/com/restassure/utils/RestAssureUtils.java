package com.restassure.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import com.apimodals.UserPOJO;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class RestAssureUtils {
	
	ObjectMapper mapper = new ObjectMapper();
	public static RequestSpecification requestSpec;
	public static ResponseSpecification responseSpec;
	private String basePath = RestAssureUtils.getProperty("BaseURL");
	
	public void responseSpecification() {
		responseSpec = new ResponseSpecBuilder().expectStatusCode(200).build();
	}
	
	public void requestSpecification() {
		requestSpec = new RequestSpecBuilder().setContentType(ContentType.JSON).setBaseUri(basePath).build();
//	System.out.println(requestSpec.log().all());
	}
	
	public static String getProperty(String key) {
		String filePath = System.getProperty("user.dir") + "//src//main//resources//config.properties";
		FileInputStream fis = null;
		Properties configProperty = null;
		try {
			fis = new FileInputStream(new File(filePath));

			configProperty = new Properties();
			configProperty.load(fis);
			return configProperty.getProperty(key);
		} catch (IOException ioe) {
			ioe.printStackTrace();
			return "";
		} finally {
			// write code if needed.
		}

	}
	
	//need to upgrade this methods usage
	//need to define best usage in the tests.

	public  Object convertsFromJsonToBean(String filePath) throws JsonParseException, JsonMappingException, IOException {
		Object obj = mapper.readValue(new File(filePath), Object.class);
		return obj;

	}

}
