package com.restassured.APIhelpers;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.testng.Assert;

import com.apimodals.PhotosPOJO;
import com.commondata.pool.DataStorage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.restassure.utils.RestAssureUtils;
import com.restassurepract.endpoints.PhotosEndPoints;

import io.restassured.response.Response;

public class PhotosAPI {

	private int photoIDIndex = 2;
	String updatePhotoJson = "UpdatePhoto.json";
	RestAssureUtils utils = new RestAssureUtils();
	ObjectMapper mapper = new ObjectMapper();
	FileInputStream fis;
	PhotosPOJO updatePhoto;

	public Response getAllPhotos() {
		Response res =given()
				.spec(RestAssureUtils.requestSpec)
				.when()
				.get(PhotosEndPoints.GET_ALL_PHOTOS);
		
		return res;
	}

	public Response getSpecificPhoto() {
		Response res = given().spec(RestAssureUtils.requestSpec)
				.get(PhotosEndPoints.GET_SPECIFIC_PHOTOS + DataStorage.PHOTOS_ID);

		return res;
	}

	public Response updateSpecificPhoto() {

		try {
			String path = System.getProperty("user.dir") + "//src//main//resources//PhotosResources//"+ updatePhotoJson;
			fis = new FileInputStream(new File(path));
			updatePhoto = mapper.readValue(fis, PhotosPOJO.class);
			updatePhoto.setThumbnailUrl("http://mythumbnailURL");
		} catch (Exception e) {
			e.printStackTrace();
		}

		Response res = given().spec(RestAssureUtils.requestSpec).body(updatePhoto).when()
				.put(PhotosEndPoints.UPDATE_SPECIFIC_PHOTOS + DataStorage.PHOTOS_ID);

		return res;
	}

	public void validateStatusCodeResponse(Response res) {
		res.then().spec(RestAssureUtils.responseSpec);
	}

	public void validateResponseAfterGetAllPhotos(Response res) {
		List<PhotosPOJO> listPhoto = res.getBody().jsonPath().getList(".");

		Assert.assertTrue(listPhoto.size() == 5000," Your Photos are missing buddy");

	}

	public void assignPhotoToCommonData(Response res) {
		DataStorage.PHOTOS_ID = res.getBody().jsonPath().getList("id").get(photoIDIndex).toString();
	
	}

	public void validateResponseBodyAfterGetSpecificPhoto(Response res) {
		boolean jsonDataCompare;
		try {
			fis = new FileInputStream(
					new File(System.getProperty("user.dir") + "//src//main//resources//PhotosResources//"+updatePhotoJson));
			jsonDataCompare = mapper.readTree(res.getBody().asInputStream()).equals(mapper.readTree(fis));
			Assert.assertTrue(jsonDataCompare, "Json response for photo is not matched with the expected json");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void validateResponseBodyAfterUpdatePhoto(Response res) {
		updatePhoto=res.as(PhotosPOJO.class);
		Assert.assertTrue(updatePhoto.getThumbnailUrl().equals("http://mythumbnailURL"),"Photo is not updated like expected.");
	
	
	
	}
}
