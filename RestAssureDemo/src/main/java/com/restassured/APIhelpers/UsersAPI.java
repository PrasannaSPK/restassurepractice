package com.restassured.APIhelpers;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.apimodals.UserPOJO;
import com.commondata.pool.DataStorage;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.restassure.utils.RestAssureUtils;
import com.restassurepract.endpoints.UsersEndPoints;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

/**
 * @author sodhaprasanna.k
 *
 *         This class is having the request methods which will help to build the
 *         tests.
 *
 */
public class UsersAPI {

	ObjectMapper mapper = new ObjectMapper();

	private String updateUserJson = "UserUpdate.json";
	private FileInputStream fis;
	private UserPOJO updateUser;

	public void validateStatusCodeResponse(Response res) {
		res.then().spec(RestAssureUtils.responseSpec);
	}

	public Response getAllUsers() {
		Response response = given(RestAssureUtils.requestSpec)
				.when()
				.get(UsersEndPoints.GET_ALL_USERS);

		return response;
	}

	public Response getSpecificUser() {
		Response response = given(RestAssureUtils.requestSpec).when()
				.get(UsersEndPoints.GET_SINGLE_USER + DataStorage.USER_ID);
		return response;
	}

	public boolean validateReponseBodyWithExpected(Response response, String expectedJsonFileName) {

		try {
			fis = new FileInputStream(new File(
					System.getProperty("user.dir") + "//src//main//resources//userresources//" + expectedJsonFileName));
			return mapper.readTree(response.getBody().asString()).equals(mapper.readTree(fis));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {

			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;

	}

	public Response updateSpecificUser() {

		try {
			fis = new FileInputStream(new File(
					System.getProperty("user.dir") + "//src//main//resources//userresources//" + updateUserJson));
			updateUser = mapper.readValue(fis, UserPOJO.class);
			updateUser.setWebsite("www.spk.com");
		} catch (FileNotFoundException fne) {
			fne.printStackTrace();
		} catch (JsonParseException e) {

			e.printStackTrace();
		} catch (JsonMappingException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		Response response = given().spec(RestAssureUtils.requestSpec).body(updateUser)
				.put(UsersEndPoints.UPDATE_USER + DataStorage.USER_ID);
		return response;
	}

	public boolean validateResponseBodyAfterUpdatingUser(Response res) {
		updateUser = res.getBody().as(UserPOJO.class);

		return (updateUser.getId() == Integer.parseInt(DataStorage.USER_ID));
	}

}
