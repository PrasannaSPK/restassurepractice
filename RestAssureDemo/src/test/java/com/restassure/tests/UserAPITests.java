package com.restassure.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.commondata.pool.DataStorage;
import com.restassure.utils.RestAssureUtils;
import com.restassured.APIhelpers.UsersAPI;

import io.restassured.response.Response;

/**
 * @author sodhaprasanna.k
 * This class is having the tests related to users API.
 */
public class UserAPITests {

	UsersAPI user=new UsersAPI();
	RestAssureUtils utils=new RestAssureUtils();
	Response response;
	
	@BeforeClass
	public void loadSpecs() {
		
		utils.requestSpecification();
		utils.responseSpecification();
	}
	
	@Test(priority=0)
	public void getAllUserTest() {
	System.out.println("User test 1");

	// 
	response=user.getAllUsers();
	// 
	user.validateStatusCodeResponse(response);
	 //
	DataStorage.USER_ID=response.getBody().jsonPath().getList("id").get(2).toString();
	}
	@Test(priority=1)
	public void getSpecificUser() {
		System.out.println("User test 2");

		response=user.getSpecificUser();
		//
		user.validateStatusCodeResponse(response);
		//
		Assert.assertTrue(user.validateReponseBodyWithExpected(response, "UserUpdate.json"), "json data is not as expected");
	
	
	}
	
	@Test(priority=2)
	public void updateSpecificUser() {
		System.out.println("User test 3");

		//
		response=user.updateSpecificUser();
		//
		user.validateStatusCodeResponse(response);
		//
		Assert.assertTrue(user.validateResponseBodyAfterUpdatingUser(response),"user is not updated as expected");
	}
	
}
