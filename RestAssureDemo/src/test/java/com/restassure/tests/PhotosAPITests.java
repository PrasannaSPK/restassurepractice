package com.restassure.tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.restassure.utils.RestAssureUtils;
import com.restassured.APIhelpers.PhotosAPI;

import io.restassured.http.Header;
import io.restassured.response.Response;

public class PhotosAPITests {

	PhotosAPI photos = new PhotosAPI();
	RestAssureUtils utils = new RestAssureUtils();

	@BeforeClass
	public void loadSpecs() {

		utils.requestSpecification();
		utils.responseSpecification();
	}

	@Test(priority = 0)
	public void getAllPhotosTest() {
		System.out.println("Photos test 1");
		Response res = photos.getAllPhotos();

		photos.validateStatusCodeResponse(res);

		photos.validateResponseAfterGetAllPhotos(res);

		photos.assignPhotoToCommonData(res);
	}

	@Test(priority = 1)
	public void getSpecificPhotoTest() {
		System.out.println("Photos test 2");

		Response res = photos.getSpecificPhoto();

		photos.validateStatusCodeResponse(res);

		photos.validateResponseBodyAfterGetSpecificPhoto(res);

	}
	@Test(priority = 2)
	public void updateSpecificPhotoTest() {
		System.out.println("Photos test 3");

		Response res = photos.updateSpecificPhoto();

		photos.validateStatusCodeResponse(res);

		/*
		 * for(Header header:res.getHeaders().asList()) {
		 * System.out.println(header.getName()+"------>"+header.getValue()); }
		 */

	}
}
